# Imports the Google Cloud client library
from google.cloud import storage

# Specify bucket and object name
bucket_name = "gcpdigitalplatform"
blob_name = 'cloud.jpeg'

storage_client = storage.Client()

bucket = storage_client.bucket(bucket_name)
blob = bucket.get_blob(blob_name)

# Update object level custom metadata key:values 
metadata = {'tag': 'deletenow', 'name': 'test'}
blob.metadata = metadata
blob.patch()

print("The metadata for the blob {} is {}".format(blob.name, blob.metadata))
