# Imports the Google Cloud client library
import json
from google.cloud import storage

# Specify bucket and object name
bucket_name = "gcpdigitalplatform"
blob_name = 'cloud.jpeg'

storage_client = storage.Client()

bucket = storage_client.bucket(bucket_name)
blob = bucket.get_blob(blob_name)

# Get existing Metadata
print("Metadata: {}".format(blob.metadata))

# Parse the metadata to get the 'tag' value
metadata = blob.metadata
metadata_json = json.dumps(metadata)
metadata_parsed = json.loads(metadata_json)

status = metadata_parsed["tag"]

if status == "deletenow":
    blob.delete()
    print("Blob {} deleted.".format(blob_name))

print("Object Deleted")    
